
myftpd: myftpd.o common.o stream.o
	gcc myftpd.o common.o stream.o -o myftpd

myftpd.o: myftpd.c stream.c common.c
	gcc -c myftpd.c

common.o: common.c common.h
	gcc -c common.c

stream.o: stream.c stream.h
	gcc -c stream.c

clean: 
	rm *.o