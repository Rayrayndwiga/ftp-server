#include <stdio.h>
#include <stdlib.h>
#include <string.h> 		/* strlen() strcmp() */
#include <errno.h> 			/* extern int errno, EINTR, perror() */
#include <signal.h>			/* SIGCHLD, sigaction() */
#include <sys/types.h>		/* pid_t, u_long, u_short */
#include <sys/stat.h>		/* umask() */
#include <sys/socket.h>		/* struct sockaddre, socket() */
#include <sys/wait.h>		/* waitpid(), WNOHAND */
#include <netinet/in.h>		/* struct sockaddr_in, htons(), htonl(), INADDRE_ANY */
#include "stream.h"     	/* MAX_BLOCK_SIZE, readn(), writen() */
#include <unistd.h>			/* close() */
#include <dirent.h>
#include  <fcntl.h>
#include "common.h"			/* file_exists(), directory_exists() */

#define BUFSIZE 				512			/**/

#define SERVER_PORT 			40190		/* Port Number for the server */
#define SERVICESUPPORTED		50			/* Service requested by client is supported */
#define SERVICENOTSUPPORTED		75			/* Service requested by client is not supported */
#define STARTTRANSFER			90			/* Start transfer of file */
#define ENDTRANSFER				95			/* End of file transfer */
#define SENDFILE				100			/* Opcode for sending a file from the server to the client */
#define GETFILE					200			/* Opcode for getting a file from the client to the server */
#define GOODFILE				300			/* Opcode to show file requested can be sent. */
#define GOODDIR					350			/* Opcode to show there is a directory existing with the name given. */
#define NOFILE					400			/* Opcode to show there isnt any file existing with the name given. */
#define NODIRECTORY				450			/* Opcode to show there isnt any directory existing with the name given. */
#define CLASHFILE				500			/* Opcode to show the file being sent already exists. */
#define BADFILE					600			/* Opcode to show file cannot be accepted cause of other reasons. */
#define ACKN					5			/* Opcode to show acknowledgment was received. */
#define NACKN					6			/* Opcode to show acknowledgement was not received. */
#define MAX_SEND_SIZE 			(1024*5) 	/* Maximum size of data that can be sent in one go */
#define SERVERPWD				700			/* Opcode to find out the current directory of the server */
#define SERVERDIR				750			/* Opcode to list the contents of the servers current directory */
#define SERVERCD				800			/* Opcode to change directory in the server */

char current_directory[BUFSIZE];
/**
*This fucntion is used to claim zombie processes so that the zombie
*processes do not use up the process table.
*
*/
void claim_children()
{
	pid_t pid = 1;
	while (pid > 0){
		pid = waitpid(0,(int *)0, WNOHANG);
	}
}

/**
*This fucntion is used to make the current running process into a daemon process.
*/
void daemon_init(void)
{
	pid_t pid;
	struct sigaction kill_zombies;
	
	if ( (pid = fork() ) < 0){
		perror("Forking error");
		exit(1);
	}
	else if (pid > 0) {
		printf("server pid=%d\n", pid); 
		exit(0);
	}
	/* child continues */
   	setsid(); 		/*Become session leader*/

   	chdir("/"); 	/*Change working directory*/

   	umask(0);		/*clear our file mode creation mask*/

	/* Catch SIGCHLD to remove zombies in the program */
	kill_zombies.sa_handler = claim_children;
	sigemptyset(&kill_zombies.sa_mask);
	kill_zombies.sa_flags = SA_NOCLDSTOP; 
}
/**
* This function is used to show the current directory of the server.
* socket is the socket of the client being served
*/
void pwd(int socket)
{
	char dir[MAX_SEND_SIZE];

	getcwd(dir,MAX_SEND_SIZE);
	if (writen(socket, dir, MAX_SEND_SIZE) < MAX_SEND_SIZE ){
		return;//("Server: error writing current working directory to client");
	}
}
/*
*
*	socket is the socket of the client being served
*/
void dir(int socket)
{
	DIR *dir;
	struct dirent *direntdir;
	int num_of_files = 0 ;
	char listing[MAX_SEND_SIZE];
	char currentdir[20] = ".";
	
	dir = opendir(currentdir);
	
	while((direntdir = readdir(dir)) != NULL){
		num_of_files++;
	}
	
	num_of_files = htons(num_of_files);
	if (write(socket, (char *) &num_of_files, sizeof(num_of_files)) <= 0 ){
		return;//("Server:error writing number of files in directory. \n");
	}
	
	
	rewinddir(dir);
	while((direntdir = readdir(dir)) != NULL){
		listing[0] = '\0';
		strcpy(listing, direntdir->d_name);
		
		if ( writen(socket,listing,MAX_SEND_SIZE) < MAX_SEND_SIZE){
			return;//("Server:error writing file/directory name to client");
		}
		
		
	}
	
	closedir(dir);
}
/*
*	This function is used to change directory in the server.
*	socket is the socket of the client being served
*/
void cd(int socket)
{
	char dirname[MAX_SEND_SIZE];
	int file_desc, dir_status = 0, result;
	
	
	dirname[0] = '\0';
	/* Get directory name from client */
	if ( readn(socket, dirname, MAX_SEND_SIZE) <= 0 ){
		return;//("Server:error reading directory name from client.\n");
	} 
	
	
	if (!directory_exists(dirname)){
		dir_status = NODIRECTORY;
		dir_status = NODIRECTORY;
		dir_status = htons(dir_status);
		if (write (socket,(char *) &dir_status, sizeof(dir_status)) <= 0 ){
			return;//("Server: errror writing directory status.\n");
		}
	}
	else{
		
		result = chdir(dirname);
		if (result == 0 ){
			dir_status = GOODDIR;
			dir_status = htons(dir_status);
			if (write (socket,(char *) &dir_status, sizeof(dir_status)) <= 0 ){
				return;//("Server: errror writing directory status.\n");
			}
		}
		else{
			dir_status = NODIRECTORY;
			dir_status = htons(dir_status);
			if (write (socket,(char *) &dir_status, sizeof(dir_status)) <= 0 ){
				return;//("Server: errror writing directory status.\n");
			}
		}
		
	}
}

/**
* purpose:This function is used to send a file from the server to the client
*	 socket is the socket of the client being served
*/
void ftp_service_to_client(int socket)
{
	
	int file_status, start_transfer, file_size, file_name_size, file_read, response, end_transfer;
	int num_blks, num_blks_byte, num_lstbk, num_lstblk_byte, i, file_no_read;
	char file_out_buf[MAX_SEND_SIZE], tmp_fname[MAX_SEND_SIZE];
	FILE *file;
	
	
	
	
	/* Get size of file name from client */
	file_name_size = 0;
	if ( read (socket, (char *) &file_name_size, sizeof(file_name_size)) <= 0){
		return; // Server: read filename size error
	}
	file_name_size = ntohs(file_name_size);
	char file_name[file_name_size]; // create array to store filename
	
	/* Let the client know that the server received the filename size */
	response = ACKN;
	response = htons(response);
	if ( write(socket,(char *) &response, sizeof(response)) <= 0){
		return;//Error sending ackn of file size to client.
	}
	/* Get filename the client is requesting from the server. */
	if (readn(socket, tmp_fname, MAX_SEND_SIZE) <= 0 ){
		return; // Server: read file name error.
	}
	strcpy(file_name, tmp_fname);
	memset(tmp_fname, '\0', MAX_SEND_SIZE);
	/* Check if the file exists and let the client know */
	if( !file_exists(file_name)){ // if the file does not exist
		file_status = NOFILE;
		file_status = htons(file_status);
		if (write(socket, (char*) &file_status, sizeof(file_status)) <= 0){
			return; // Server: write file doesnt exist exist.
		}
		return; // Server: file doesnt exist.
	}
	else{
		file = fopen(file_name,"r");
	}
	
	/* If the file exists */
	file_status = GOODFILE;
	file_status = htons(file_status);
	if (write(socket, (char *) &file_status,sizeof(file_status)) <= 0 ){
		return; // Server: file exists write error.
	}
	
	/* Get the start transfer request from the client */
	start_transfer = 0;
	if ( read(socket, (char *) &start_transfer, sizeof(start_transfer)) <= 0){
		return; // Server: read start transfer request error.
	}
	start_transfer = ntohs(start_transfer);
	
	if( start_transfer == STARTTRANSFER ){
		file_size = 0;
		/* Get the size of file to be sent and tell client size to expect. */
		
		
		fseek(file, 0L, SEEK_END);
		file_size = ftell(file);
		/* Send the file size and wait for acknowledgment from client. */
		file_size = htons(file_size);
		if ( write(socket, (char *) &file_size, sizeof(file_size) ) <= 0){
			return; // Server: read start transfer request error.
		}
		
		response = 0;
		/* Get response from client about acknowledgment of file_size */
		if ( read(socket, (char *) &response, sizeof(response) ) <= 0 ){
			return; // Server: read start transfer request error.
		}
		
		if ( ntohs(response) != ACKN ){
			return; // Server: size ackn not received.
		}
		/* Place cursor at the beginning of file */
		fseek(file, 0L, SEEK_SET);
		
		
		/* Begin file transfer */
		bzero(file_out_buf, MAX_SEND_SIZE); 
		while((file_no_read = fread(file_out_buf, sizeof(char), MAX_SEND_SIZE, file)) > 0){
			if(send(socket, file_out_buf, file_no_read, 0) < 0)
			{
				return;//("ERROR: Failed to send file . (errno = %d)\n",  errno);
				break;
			}
			bzero(file_out_buf, MAX_SEND_SIZE); 
			
		}
		
		/* Transfer of file has ended */
		response = 0;
		if ( read(socket,(char *) &response, sizeof(response)) <= 0 ){
			printf("Client: error receiving end of transfer command.\n");
		}
		response = ntohs(response);
		if (response == ENDTRANSFER ){
			fclose(file);
		}
	}
}


/**
* This fucntion is used to receive a file from the client.
*	socket is the socket of the client being served
*/
void ftp_service_to_server(int socket)
{
	char  client_buf[MAX_SEND_SIZE], tmp_fname[MAX_SEND_SIZE];
	int file_status, start_transfer, file_size, file_name_size, file_read, response, end_transfer, amount_written;
	FILE *file;
	
	
	/* Get size of file name from client */
	file_name_size = 0;
	if ( read (socket, (char *) &file_name_size, sizeof(file_name_size)) <= 0){
		return; // Server: read filename size error
	}
	file_name_size = ntohs(file_name_size);
	char file_name[file_name_size]; // create array to store filename
	
	/* Let the client know that the server received the filename size */
	response = ACKN;
	response = htons(response);
	if ( write(socket,(char *) &response, sizeof(response)) <= 0){
		return;//("Error sending ackn of file size to client.");
	}
	/* Get filename the client wants to send to the server. */
	if (readn(socket, tmp_fname, MAX_SEND_SIZE) <= 0 ){
		printf("Server: read file name error.");
		return; // Server: read file name error.
	}
	strcpy(file_name, tmp_fname);
	memset(tmp_fname, '\0', MAX_SEND_SIZE);
	/* Check if the file exists and let the client know */
	if( file_exists(file_name)){ // if the file exists
		file_status = NOFILE;
		file_status = htons(file_status);
		if (write(socket, (char*) &file_status, sizeof(file_status)) <= 0){
			return; // Server: write file  exists.
		}
		return; // Server: file exists.
	}

	
	/* If the file doesnt exist */
	file_status = GOODFILE;
	file_status = htons(file_status);
	if (write(socket, (char *) &file_status,sizeof(file_status)) <= 0 ){
		return; // Server: file doesnt exist write error.
	}
	
	/* Get the start transfer request from the client */
	start_transfer = 0;
	if ( read(socket, (char *) &start_transfer, sizeof(start_transfer)) <= 0){
		return; // Server: read start transfer request error.
	}
	start_transfer = ntohs(start_transfer);
	
	/* Send ackn of receivng start transfer code */
	response = ACKN;
	response = htons(response);
	if (write(socket, (char *) &response, sizeof(response)) <= 0 ){
		return;//("Client:error sending ackn of file size. \n");
	}
	
	
	if ( start_transfer == STARTTRANSFER){
		
		/* Get the filesize from the client */
		file_size = 0;
		if (read(socket, (char *) &file_size, sizeof(file_size)) <= 0){
			return;//("Client:error reading file size from server. \n");
		}
		/* Send ackn of receivng filesize */
		response = ACKN;
		response = htons(response);
		if (write(socket, (char *) &response, sizeof(response)) <= 0 ){
			return;//("Client:error sending ackn of file size. \n");
		}
		//open file to write
		file = fopen(file_name,"w");
		/* File transfer begins */
		bzero(client_buf, MAX_SEND_SIZE); 
		int amount_rec = 0;
		while((amount_rec = recv(socket, client_buf, MAX_SEND_SIZE, 0)) > 0) 
		{
			amount_written = fwrite(client_buf, sizeof(char), amount_rec, file);
			if(amount_written < amount_rec)
			{
				return;//("Server:File write failed on server.\n");
			}
			bzero(client_buf, MAX_SEND_SIZE);
			if (amount_rec == 0 || amount_rec != MAX_SEND_SIZE) 
			{
				break;
			}
		}


		/* Transfer of file has ended */
		end_transfer = ENDTRANSFER;
		end_transfer = htons(end_transfer);
		// Write out to client that the file transfer has ended
		if (write(socket, (char *) &end_transfer, sizeof(end_transfer)) <= 0 ){
			return; // Server: send end transfer error.
		}
		fclose(file);
		
	}
	
	
}

/**
* This method is used to serve each individual client after the main process forks
* of a child process.
*	socket is the socket of the client being served
*/
void serve_a_client(int sd)
{
	int client_command_read,client_command_write ,i = 0;
	
	
	while(++i)
	{
		
		client_command_read = 0;
		/* Read which service the client wants to use */
		if(( read(sd,(char *) &client_command_read, sizeof(client_command_read))) <= 0 ){
			return;//("server: read service error %d",errno);
		}
		client_command_read = ntohs(client_command_read);
		
		if (client_command_read != SENDFILE && client_command_read != GETFILE && client_command_read != SERVERPWD && client_command_read != SERVERDIR && client_command_read != SERVERCD){
			return;//("Server: Unsupported service");
			/* Let the client know the service they requested is not supported with OPCODE 75*/
			client_command_write = SERVICENOTSUPPORTED;
			client_command_write = htons(client_command_write);
			if ( write(sd, (char *) &client_command_write, sizeof(client_command_write) ) <= 0 ){
				return;//("Server: service not supported write error");
			}
		}
		
		/* If service is supported let the client know that they can use that service */
		client_command_write = SERVICESUPPORTED;
		client_command_write = htons(client_command_write);
		if ( write(sd, (char *) &client_command_write, sizeof(client_command_write)) <= 0 ){
			return;//("Server: service supported write error");
		}
		
		/*Do the ftp service according which one the clients wants to use */
		switch(client_command_read){
			case SERVERPWD:// show current working directory
			pwd(sd);
			break;
			
			case SERVERDIR: //list contents of directory
			dir(sd);
			break;
			
			case SERVERCD: // change directory in server
			cd(sd);
			break;
			
			case GETFILE: // client requesting a file from the server
			ftp_service_to_client(sd);
			break;
			
			case SENDFILE: //sending a file to the server from the client
			ftp_service_to_server(sd);
			break;
			
			default:
			break;
		}
		
	}
}

int main()
{
	int sd, nsd, n;
	socklen_t client_addr_len;
	pid_t pid;
	struct sockaddr_in server_addr, client_addr;
	
	
	/* Swtich the program into a daemon */
	daemon_init();
	
	/* Create the server socket*/
	if((sd = socket(AF_INET, SOCK_STREAM, 0)) < 0){
		perror("Error in establishing server socket");
		exit(1);
	}
	/* Build server Internet socket address */
	bzero((char *)&server_addr, sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(SERVER_PORT);
	server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	
	/* Bind server address to socket */
	if (bind(sd, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0 ){
		perror("Error in binding server address to socket");
	}
	/* Switch to become a listening socket */
	if(listen(sd,5)){
		perror("Error in setting listening socket");
	}
	
	/* Run as concurrent server to handle multiple clients */
	while(1){
		
		/* Wait to accept a client request for connection */
		client_addr_len = sizeof(client_addr);
		
		nsd = accept(sd, (struct sockaddr *) &client_addr, &client_addr_len);
		if (nsd < 0){
			if (errno == EINTR){	// if interrupted by SIGCHLD
				continue;
			} 
			perror("Error accepting client connection");
		}
		
		/* Creat child process to server the current client */
		if ((pid = fork()) < 0){
			perror("Fork failed");
			exit(1);
		}
		else if(pid > 0){
			close(nsd);
			continue; // parent to wait for the next client
		}
		
		/* In the child now */
		close(sd);
		serve_a_client(nsd);
		exit(0);
	}
	
	return 0;
}

